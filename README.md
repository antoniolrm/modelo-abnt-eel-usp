# Modelo ABNT EEL-USP

Modelo de tese da Escola de Engenharia de Lorena - Universidade de São Paulo baseado em [abnTeX2](https://github.com/abntex/abntex2)

## Requisitos
Existem várias dependências. Para instalar todas de forma rápida, a minha sugestão é
```
sudo apt-get install texlive-full
```

## Agradecimentos
Agradecimento especial aos desenvolvedores do [abnTeX2](https://github.com/abntex/abntex2).
